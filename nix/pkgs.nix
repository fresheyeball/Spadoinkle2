let

  rev = "153d32cd9fac7e885979426b0e86b560a661a1ac";

  doJailbreak = pkgs.haskell.lib.doJailbreak;

  overlay = self: super: {
    haskellPackages = super.haskell.packages.ghc843.override {
      overrides = hself: hsuper: {
        jsaddle-warp = doJailbreak super.haskell.packages.ghc843.jsaddle-warp;
      };
    };
  };

  pkgs = import
  (builtins.fetchTarball { url = "https://github.com/NixOS/nixpkgs/archive/${rev}.tar.gz"; })
  { overlays = [ overlay ]; };

in pkgs
